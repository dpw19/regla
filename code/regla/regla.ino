/*
 * Regla inalámbrica ultrasónica
 * 
 * Autor: Pavel E Vazquez Mtz pavel_e@riseup.net
 * 
 * Date: 12/01/2023
 * 
 */


#include <LiquidCrystal.h>
//Define displa pines
const int RS = 12, EN = 11, D4 = 5, D5 = 4, D6 = 3, D7 = 2;
//Define Ultrasonic Sensor pines
const int TRIG = 7, ECHO = 6;

long Tiempo;
float Distancia;

// Display object in 4 bit's mode
LiquidCrystal lcd(RS,EN,D4,D5,D6,D7); 

void   setup() {
//  Serial.begin(19200);
  
  pinMode(ECHO,INPUT);
  pinMode(TRIG,OUTPUT);

//Initialize display in 16x2 mode
  lcd.begin(16,2);
  lcd.print("   REGLA v0.1");
  lcd.setCursor(0,1);
  lcd.print("Dist = ");
}

void loop() {
  calculoDistancia();
  imprimeDistancia();
  delay(1000);
}

void calculoDistancia(void){
  digitalWrite(TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG, LOW);

  Tiempo = (pulseIn(ECHO, HIGH)/2);
  Distancia = float(Tiempo * 0.0343);
}

void imprimeDistancia(void){
  lcd.setCursor(8,1);
  lcd.print(Distancia);
  lcd.print(" CM ");
}
